package co.shapen.servasapp.interfaces;

import android.view.View;

/**
 * Created by Shapen on 7/26/2017.
 */

public interface CustomOnitemClick {

    public void setOnItemClickListner(View v, int position);
}
