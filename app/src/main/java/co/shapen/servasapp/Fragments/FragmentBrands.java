package co.shapen.servasapp.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import co.shapen.servasapp.Adapter.RecycleViewAllGridItemsAdapter;
import co.shapen.servasapp.Models.SubListItems;
import co.shapen.servasapp.R;
import co.shapen.servasapp.interfaces.CustomClick;

/**
 * Created by Shapen on 7/25/2017.
 */

public class FragmentBrands extends Fragment {

    List<SubListItems> subListItems = new ArrayList<SubListItems>();
    RecyclerView recyclerView;
    TextView tv;
    CustomClick customClick;
    View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_view_all, container, false);

        populateData();
        linkXml(view);

        return view;

    }

    private void linkXml(View view) {
        customClick =  new CustomClick() {
            @Override
            public void setOnClickListner(View v, int position) {

            }
        };
        recyclerView = (RecyclerView) view.findViewById(R.id.recycleview_grid);
        tv = (TextView) view.findViewById(R.id.product_title);

        tv.setText("Brands");
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        int numberOfColumns = 2;
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), numberOfColumns));
        recyclerView.setAdapter(new RecycleViewAllGridItemsAdapter(getActivity(), subListItems, customClick));

    }


    private void populateData() {

        subListItems.clear();
        SubListItems category = new SubListItems(R.mipmap.brand1, "Junaid Jamshed", null, null, null);
        subListItems.add(category);
        category = new SubListItems(R.mipmap.brand2, "Almirah", null, null, null);
        subListItems.add(category);
        category = new SubListItems(R.mipmap.brand3, "Gul Ahmed", null, null, null);
        subListItems.add(category);
        category = new SubListItems(R.mipmap.brand4, "Khaadi", null, null, null);
        subListItems.add(category);
        category = new SubListItems(R.mipmap.brand5, "Sana Safinaz", null, null, null);
        subListItems.add(category);
        category = new SubListItems(R.mipmap.brand6, "Levi's", null, null, null);
        subListItems.add(category);
    }
}
