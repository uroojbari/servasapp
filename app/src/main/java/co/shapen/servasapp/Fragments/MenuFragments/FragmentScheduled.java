package co.shapen.servasapp.Fragments.MenuFragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import co.shapen.servasapp.Adapter.RecycleBookingAdapter;
import co.shapen.servasapp.Adapter.RecycleViewCategoryGridItemsAdapter;
import co.shapen.servasapp.Models.SubListItems;
import co.shapen.servasapp.R;

/**
 * Created by Shapen on 7/25/2017.
 */

public class FragmentScheduled extends Fragment {
    RecyclerView recyclerView;
    View view;
    RecycleBookingAdapter recycleBookingAdapter;
    RecycleViewCategoryGridItemsAdapter adapter;
    TextView tv;
    List<SubListItems> data = new ArrayList<SubListItems>();
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_recycle_view, container, false);
        linkXml(view);
        populateData();
        return view;
    }



    private void linkXml(View view) {

        recyclerView = (RecyclerView) view.findViewById(R.id.recycling_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(new RecycleBookingAdapter(getActivity(),data));


    }
    private void populateData() {
        data.clear();
        SubListItems dataitems = new SubListItems("Bank al Habib","Aga Khan Hospital");
        data.add(dataitems);
        dataitems = new SubListItems("Bank al Habib","Aga Khan Hospital");
        data.add(dataitems);
        dataitems = new SubListItems("Bank al Habib","Aga Khan Hospital");
        data.add(dataitems);
        data.add(dataitems);
        dataitems = new SubListItems("Bank al Habib","Aga Khan Hospital");
        data.add(dataitems);
        data.add(dataitems);
        dataitems = new SubListItems("Bank al Habib","Aga Khan Hospital");
        data.add(dataitems);
        dataitems = new SubListItems("Bank al Habib","Aga Khan Hospital");
        data.add(dataitems);
        dataitems = new SubListItems("Bank al Habib","Aga Khan Hospital");
        data.add(dataitems);

    }
}
