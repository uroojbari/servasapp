package co.shapen.servasapp.Fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import co.shapen.servasapp.Adapter.RecycleHomeItemsAdapter;
import co.shapen.servasapp.Adapter.RecycleServicesDetailsAdapter;
import co.shapen.servasapp.Adapter.RecycleServicesInfoAdapter;
import co.shapen.servasapp.Models.SubListItems;
import co.shapen.servasapp.Models.Sublist;
import co.shapen.servasapp.R;
import co.shapen.servasapp.Utils.SlidingImage_Adapter;
import co.shapen.servasapp.interfaces.CustomClick;

/***
 * Created by Shapen on 7/21/2017.
***/
public class FragmentServicesDetails extends Fragment {
    View view;
    int numberOfColumns = 2;
    RecyclerView recycle_info, recycle_great_for, recycle_included, recycle_price;
    List<SubListItems> services_info = new ArrayList<SubListItems>();
    List<SubListItems> services_great_for = new ArrayList<SubListItems>();
    List<SubListItems> services_included = new ArrayList<SubListItems>();
    List<SubListItems> services_price = new ArrayList<SubListItems>();
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_service_details, container, false);
        populateData();
        linkXML(view);
        return view;
    }

    private void linkXML(View view) {
        recycle_info = (RecyclerView) view.findViewById(R.id.service_details1);
        recycle_info.setLayoutManager(new GridLayoutManager(getActivity(), numberOfColumns));
        recycle_info.setAdapter(new RecycleServicesDetailsAdapter(getActivity(), services_info));

        recycle_great_for = (RecyclerView) view.findViewById(R.id.service_details2);
        recycle_great_for.setLayoutManager(new LinearLayoutManager(recycle_great_for.getContext()));
        recycle_great_for.setAdapter(new RecycleServicesDetailsAdapter(getActivity(), services_great_for));

        recycle_included = (RecyclerView) view.findViewById(R.id.service_details3);
        recycle_included.setLayoutManager(new LinearLayoutManager(recycle_included.getContext()));
        recycle_included.setAdapter(new RecycleServicesDetailsAdapter(getActivity(), services_included));

        recycle_price = (RecyclerView) view.findViewById(R.id.service_details4);
        recycle_price.setLayoutManager(new LinearLayoutManager(recycle_price.getContext()));
        recycle_price.setAdapter(new RecycleServicesInfoAdapter(getActivity(), services_price));
    }

    private void populateData() {

        services_info.clear();
        SubListItems service_category = new SubListItems(R.mipmap.tick,null, null,null,"Instant Confirmation");
        services_info.add(service_category);
        service_category = new SubListItems(R.mipmap.tick,"2 Man team");
        services_info.add(service_category);
        service_category = new SubListItems(R.mipmap.tick,"7 Days/Week Support");
        services_info.add(service_category);
        service_category = new SubListItems(R.mipmap.tick,"30 Days Warranty");
        services_info.add(service_category);
        service_category = new SubListItems(R.mipmap.tick,"Professional Quality Service");
        services_info.add(service_category);
        service_category = new SubListItems(R.mipmap.tick,"Book Now, Pay on Delivery");
        services_info.add(service_category);

        services_great_for.clear();
        SubListItems popular = new SubListItems(R.mipmap.star_bullet,null,null,null,"Fixing Doors");
        services_great_for.add(popular);
        popular = new SubListItems(R.mipmap.star_bullet,"Curtains");
        services_great_for.add(popular);
        popular = new SubListItems(R.mipmap.star_bullet,"Furniture Assembly");
        services_great_for.add(popular);
        popular = new SubListItems(R.mipmap.star_bullet,"Flooring Carpet");
        services_great_for.add(popular);
        popular = new SubListItems(R.mipmap.star_bullet,"Handing & Mountains");
        services_great_for.add(popular);

        services_included.clear();
        SubListItems everyday = new SubListItems(R.mipmap.star_bullet,"Team of 2 handyman");
        services_included.add(everyday);
        everyday = new SubListItems(R.mipmap.star_bullet,"Basic tools (drill, screws, ladder etc.) will be brought by the team");
        services_included.add(everyday);
        everyday = new SubListItems(R.mipmap.star_bullet,"30 day warranty on the work done");
        services_included.add(everyday);

        services_price.clear();
        SubListItems monthly = new SubListItems(null,"Example 1.",null,null,"Team work of 1.5 hours. Total price: AED 225");
        services_price.add(monthly);
        monthly = new SubListItems(null,"Example 2.",null,null,"Team work of 30 min. Minimum charge will applied. Total price: AED 99");
        services_price.add(monthly);

    }


}
