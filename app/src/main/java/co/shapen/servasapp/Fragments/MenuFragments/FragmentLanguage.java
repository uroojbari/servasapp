package co.shapen.servasapp.Fragments.MenuFragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import co.shapen.servasapp.Adapter.RecycleViewCategoryGridItemsAdapter;
import co.shapen.servasapp.R;

/**
 * Created by Shapen on 7/25/2017.
 */

public class FragmentLanguage extends Fragment {
    View view;
    RecycleViewCategoryGridItemsAdapter adapter;
    TextView tv;
    ListView listView;
    String [] languages = {"English", "Arabic", "German"};
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.layout_listview, container, false);
        linkXml(view);
        return view;
    }

    private void linkXml(View view) {

        listView = (ListView) view.findViewById(R.id.listview_default);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, languages);
        listView.setAdapter(adapter);
    }

}
