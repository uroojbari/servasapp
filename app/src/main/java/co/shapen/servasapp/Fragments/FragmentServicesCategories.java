package co.shapen.servasapp.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import co.shapen.servasapp.Adapter.RecycleViewCategoryGridItemsAdapter;
import co.shapen.servasapp.Models.SubListItems;
import co.shapen.servasapp.R;
import co.shapen.servasapp.interfaces.CustomClick;

/**
 * Created by Shapen on 7/25/2017.
 */

public class FragmentServicesCategories extends Fragment {

    List<SubListItems> subListItems = new ArrayList<SubListItems>();
    RecyclerView recyclerView;
    CustomClick customClick;
    ImageView banner;
    View view;
    TextView tv;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_view_all, container, false);

        populateData();
        linkXml(view);
        return view;

    }

    private void linkXml(View view) {
        int numberOfColumns = 3;
        customClick = new CustomClick() {
            @Override
            public void setOnClickListner(View v, int position) {
                FragmentManager fm = getFragmentManager();
                fm.beginTransaction().add(R.id.fragmentlayout, new FragmentServicesDetails()).addToBackStack("Services Categories").commit();

            }
        };
        recyclerView = (RecyclerView) view.findViewById(R.id.recycleview_grid);
        tv = (TextView) view.findViewById(R.id.product_title);
        banner = (ImageView) view.findViewById(R.id.banner);
  //      recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), numberOfColumns));
        recyclerView.setAdapter(new RecycleViewCategoryGridItemsAdapter(getActivity(), subListItems, customClick));
        tv.setText("Services Categories");
        banner.setImageResource(R.mipmap.services_banner);
    }

    private void populateData() {

        subListItems.clear();
        SubListItems category = new SubListItems(R.mipmap.serv_1, "Plumbing", null, null, "yes");
        subListItems.add(category);
        category = new SubListItems(R.mipmap.serv_2, "Cleaning", null, null, "yes");
        subListItems.add(category);
        category = new SubListItems(R.mipmap.serv_9, "Electrician", null, null, "yes");
        subListItems.add(category);
        category = new SubListItems(R.mipmap.serv_3, "Construction", null, null, "yes");
        subListItems.add(category);
        category = new SubListItems(R.mipmap.serv_4, "AC Reparing", null, null, "yes");
        subListItems.add(category);
        category = new SubListItems(R.mipmap.serv_5, "Carpenter", null, null, "yes");
        subListItems.add(category);
        category = new SubListItems(R.mipmap.serv_6, "Mover", null, null, "yes");
        subListItems.add(category);
        category = new SubListItems(R.mipmap.serv_7, "Event & Catering ", null, null, "yes");
        subListItems.add(category);
        category = new SubListItems(R.mipmap.serv_8, "Painter", null, null, "yes");
        subListItems.add(category);

    }
}
