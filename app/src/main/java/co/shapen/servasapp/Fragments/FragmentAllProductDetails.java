package co.shapen.servasapp.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import co.shapen.servasapp.Adapter.RecycleViewAllGridItemsAdapter;
import co.shapen.servasapp.Models.SubListItems;
import co.shapen.servasapp.R;
import co.shapen.servasapp.interfaces.CustomClick;

/**
 * Created by Shapen on 7/25/2017.
 */

public class FragmentAllProductDetails extends Fragment {

    List<SubListItems> subListItems = new ArrayList<SubListItems>();
    RecyclerView recyclerView;
    TextView tv;
    CustomClick customClick;
    View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_product_details, container, false);

        populateData();
        linkXml(view);

        return view;

    }

    private void linkXml(View view) {
        customClick = new CustomClick() {
            @Override
            public void setOnClickListner(View v, int position) {

            }
        };
        String htmlAsString = getString(R.string.html);      // used by WebView
        Spanned htmlAsSpanned = Html.fromHtml(htmlAsString); // used by TextView

        // set the html content on a TextView
       // TextView textView = (TextView) findViewById(R.id.textView);
       // textView.setText(htmlAsSpanned);

        WebView webView = (WebView) view.findViewById(R.id.webView);
        webView.loadDataWithBaseURL(null, htmlAsString, "text/html", "utf-8", null);





        recyclerView = (RecyclerView) view.findViewById(R.id.related_products);
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        int numberOfColumns = subListItems.size();
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), numberOfColumns));
        recyclerView.setAdapter(new RecycleViewAllGridItemsAdapter(getActivity(), subListItems, customClick));

    }


    private void populateData() {

        subListItems.clear();
        SubListItems category = new SubListItems(R.mipmap.gift_1, "", null, null, null);
        subListItems.add(category);
        category = new SubListItems(R.mipmap.gift_2, "", null, null, null);
        subListItems.add(category);
        category = new SubListItems(R.mipmap.gift_3, "", null, null, null);
        subListItems.add(category);
        category = new SubListItems(R.mipmap.gift_4, "", null, null, null);
        subListItems.add(category);

    }

}
