package co.shapen.servasapp.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import co.shapen.servasapp.Adapter.GridHomeItemsAdapter;
import co.shapen.servasapp.Adapter.RecycleImagesAdapter;
import co.shapen.servasapp.Adapter.RecycleViewAllGridItemsAdapter;
import co.shapen.servasapp.Models.SubListItems;
import co.shapen.servasapp.R;
import co.shapen.servasapp.interfaces.CustomClick;
import co.shapen.servasapp.interfaces.CustomOnitemClick;

/**
 * Created by Shapen on 7/25/2017.
 */

public class FragmentProductDetails extends Fragment {

    List<SubListItems> subListItems = new ArrayList<SubListItems>();
    List<SubListItems> subListItems2 = new ArrayList<SubListItems>();
    RecyclerView recyclerView,  showcaseImages;
    TextView tv;
    CustomClick customClick;
    ImageView display;
    View view;
    ImageButton next, previous;
    TabLayout tabLayout;
    CustomOnitemClick customOnitemClick;
    RecycleImagesAdapter imagesAdapter;
    int count = 0;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_all_product_details, container, false);
        populateData();
        linkXml(view);
        return view;

    }

    private void linkXml(View view) {
        customClick = new CustomClick() {
            @Override
            public void setOnClickListner(View v, int position) {


            }
        };
        customOnitemClick = new CustomOnitemClick() {
            @Override
            public void setOnItemClickListner(View v, int position) {
                display.setImageResource(subListItems2.get(position).getImage());
                count = position;
            }
        };
        recyclerView = (RecyclerView) view.findViewById(R.id.related_products);
        next = (ImageButton) view.findViewById(R.id.imagenext);
        previous = (ImageButton) view.findViewById(R.id.imageprevious);
        showcaseImages = (RecyclerView) view.findViewById(R.id.grid_images);
        showcaseImages.setVisibility(View.VISIBLE);
        showcaseImages.setLayoutManager(new GridLayoutManager(getActivity(), subListItems2.size()));
        imagesAdapter = new RecycleImagesAdapter(getActivity(), subListItems2,customOnitemClick);
        showcaseImages.setAdapter(imagesAdapter);
        imagesAdapter.setOnItemClickListern(customOnitemClick);

        display = (ImageView) view.findViewById(R.id.display);

        int numberOfColumns = subListItems.size();
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), numberOfColumns));
        recyclerView.setAdapter(new RecycleViewAllGridItemsAdapter(getActivity(), subListItems, customClick));
        final WebView webView = (WebView) view.findViewById(R.id.webView);

        tabLayout = (TabLayout) view.findViewById(R.id.frame_tablayout);
        tabLayout.addTab(tabLayout.newTab().setText("Description"));
        tabLayout.addTab(tabLayout.newTab().setText("Specification"));
        tabLayout.addTab(tabLayout.newTab().setText("Return Policy"));
        tabLayout.addTab(tabLayout.newTab().setText("Reviews"));

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count++;
                if(count > subListItems2.size()-1) {
                    count = subListItems2.size()-1;

                }
                display.setImageResource(subListItems2.get(count).getImage());
            }
        });
        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                count--; //decrement our iterator
                if(count < 0) {
                    count = 0;

                }
                display.setImageResource(subListItems2.get(count).getImage());
            }
            });

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                String htmlAsString = getString(R.string.html);      // used by WebView
                //   FragmentManager fm = getFragmentManager();
                switch (tab.getPosition()) {
                    case 0:
                        webView.loadDataWithBaseURL(null, htmlAsString, "text/html", "utf-8", null);
                        //       fm.beginTransaction().add(R.id.frame_details,new FragmentProductDetails()).commit();
                        break;

                    case 1:
                        webView.loadDataWithBaseURL(null, htmlAsString, "text/html", "utf-8", null);
//                fm.beginTransaction().add(R.id.fragmentlayout,new FragmentHome()).commit();
                        break;

                    case 2:
                        webView.loadDataWithBaseURL(null, htmlAsString, "text/html", "utf-8", null);
//                fm.beginTransaction().add(R.id.fragmentlayout,new FragmentHome()).commit();
                        break;

                    case 3:
                        webView.loadDataWithBaseURL(null, htmlAsString, "text/html", "utf-8", null);
//                fm.beginTransaction().add(R.id.fragmentlayout,new FragmentHome()).commit();
                        // Toast.makeText(this, "Tab 4", Toast.LENGTH_SHORT).show();
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                String htmlAsString = getString(R.string.html);      // used by WebView

                FragmentManager fm = getFragmentManager();
                switch (tab.getPosition()) {
                    case 0:
                        webView.loadDataWithBaseURL(null, htmlAsString, "text/html", "utf-8", null);
//                        fm.beginTransaction().add(R.id.frame_details,new FragmentHome()).commit();
                        // Toast.makeText(this, "Tab 1", Toast.LENGTH_SHORT).show();
                        break;

                    case 1:
                        webView.loadDataWithBaseURL(null, htmlAsString, "text/html", "utf-8", null);
//                fm.beginTransaction().add(R.id.fragmentlayout,new FragmentHome()).commit();
                        //  Toast.makeText(this, "Tab 2", Toast.LENGTH_SHORT).show();
                        break;

                    case 2:
                        webView.loadDataWithBaseURL(null, htmlAsString, "text/html", "utf-8", null);
//                fm.beginTransaction().add(R.id.fragmentlayout,new FragmentHome()).commit();
                        // Toast.makeText(this, "Tab 3", Toast.LENGTH_SHORT).show();
                        break;

                    case 3:
                        webView.loadDataWithBaseURL(null, htmlAsString, "text/html", "utf-8", null);
//                fm.beginTransaction().add(R.id.fragmentlayout,new FragmentHome()).commit();
                        //Toast.makeText(this, "Tab 4", Toast.LENGTH_SHORT).show();
                        break;

                }
            }
        });
    }

    private void populateData() {

        subListItems.clear();
        SubListItems category = new SubListItems(R.mipmap.gift_1, "", null, null, null);
        subListItems.add(category);
        category = new SubListItems(R.mipmap.gift_2, "", null, null, null);
        subListItems.add(category);
        category = new SubListItems(R.mipmap.gift_3, "", null, null, null);
        subListItems.add(category);
        category = new SubListItems(R.mipmap.gift_4, "", null, null, null);
        subListItems.add(category);

        subListItems2.clear();
        SubListItems category1 = new SubListItems(R.mipmap.shoe1, "");
        subListItems2.add(category1);
        category1 = new SubListItems(R.mipmap.shoe2, "");
        subListItems2.add(category1);
        category1 = new SubListItems(R.mipmap.shoe3, "");
        subListItems2.add(category1);
        category1 = new SubListItems(R.mipmap.shoe4, "");
        subListItems2.add(category1);
    }

}
