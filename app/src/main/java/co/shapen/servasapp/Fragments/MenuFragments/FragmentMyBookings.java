package co.shapen.servasapp.Fragments.MenuFragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import co.shapen.servasapp.Adapter.RecycleViewCategoryGridItemsAdapter;
import co.shapen.servasapp.Fragments.FragmentHome;
import co.shapen.servasapp.R;

/**
 * Created by Shapen on 7/25/2017.
 */

public class FragmentMyBookings extends Fragment {
    TabLayout tablayout_booking;
    View view;
    RecycleViewCategoryGridItemsAdapter adapter;
    TextView tv;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_my_bookings, container, false);
        linkXml(view);
        return view;

    }

    private void linkXml(View view) {
        FragmentManager fm = getFragmentManager();
        tablayout_booking = (TabLayout) view.findViewById(R.id.tablayout_booking);
        tablayout_booking.addTab(tablayout_booking.newTab().setText("Scheduled"));
        tablayout_booking.addTab(tablayout_booking.newTab().setText("History"));
        tablayout_booking.getTabAt(0);
        fm.beginTransaction().add(R.id.tab_frame,new FragmentScheduled()).commit();


        tablayout_booking.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                FragmentManager fm = getFragmentManager();
                switch (tab.getPosition()){
                    case 0:
                        fm.beginTransaction().add(R.id.tab_frame,new FragmentScheduled()).commit();
                        break;
                    case 1:
                        fm.beginTransaction().add(R.id.tab_frame,new FragmentScheduled()).commit();
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                FragmentManager fm = getFragmentManager();
                switch (tab.getPosition()){
                    case 0:
                        fm.beginTransaction().add(R.id.tab_frame,new FragmentScheduled()).commit();
                        break;
                    case 1:
                        fm.beginTransaction().add(R.id.tab_frame,new FragmentScheduled()).commit();
                        break;
                }
            }
        });
    }

}
