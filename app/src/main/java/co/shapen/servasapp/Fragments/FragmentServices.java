package co.shapen.servasapp.Fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import co.shapen.servasapp.Adapter.RecycleHomeItemsAdapter;
import co.shapen.servasapp.Models.SubListItems;
import co.shapen.servasapp.Models.Sublist;
import co.shapen.servasapp.R;
import co.shapen.servasapp.Utils.SlidingImage_Adapter;
import co.shapen.servasapp.interfaces.CustomClick;
import co.shapen.servasapp.interfaces.CustomOnitemClick;

/***
 * Created by Shapen on 7/21/2017.
 ***/
public class FragmentServices extends Fragment {
    View view;
    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    CirclePageIndicator indicator;
    Timer swipeTimer;
    ArrayList images = new ArrayList<>();
    private static final Integer[] IMAGES= {R.mipmap.banner1,R.mipmap.banner2,R.mipmap.banner3,R.mipmap.banner2};
    private ArrayList<Integer> ImagesArray = new ArrayList<Integer>();
    List<Sublist> newSublist = new ArrayList<Sublist>();
    RecyclerView recyclerView;
    CustomOnitemClick customOnitemClick =new CustomOnitemClick() {
        @Override
        public void setOnItemClickListner(View v, int position) {
            getFragmentManager().beginTransaction().replace(R.id.fragmentlayout, new FragmentServicesDetails())
                    .addToBackStack("Services Categories").commit();
        }
    };
    CustomClick customClick = new CustomClick() {
        @Override
        public void setOnClickListner(View v, int position) {
            FragmentManager fm = getFragmentManager();
            if (newSublist.get(position).getItemTitle().equals("Services Categories")){
                fm.beginTransaction().replace(R.id.fragmentlayout, new FragmentServicesCategories()).addToBackStack("Services Categories").commit();
            }
            else{
                fm.beginTransaction().replace(R.id.fragmentlayout, new FragmentServicesDetails()).addToBackStack("Services Categories").commit();
            }
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);

        populateData();
        linkXML(view);
        init();
        onClickListerns();
        return view;
    }

    private void linkXML(View view) {

        mPager = (ViewPager) view.findViewById(R.id.pager);
        indicator = (CirclePageIndicator) view.findViewById(R.id.indicator);
        recyclerView = (RecyclerView) view.findViewById(R.id.list_home_items);
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));

    }

    private void init() {
        populatedImages();
        for(int i=0;i<images.size();i++)
            mPager.setAdapter(new SlidingImage_Adapter(getActivity(),images));
        indicator.setViewPager(mPager);
        //Set circle indicator radius
        final float density = getResources().getDisplayMetrics().density;
        indicator.setRadius(5 * density);
        images.size();
        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == images.size()) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);
        recyclerView.setAdapter(new RecycleHomeItemsAdapter(getActivity(),newSublist, customClick, customOnitemClick));

    }

    private void onClickListerns() {
        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;
            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });
    }

    public void populatedImages(){

        images.clear();

        images.add(R.mipmap.banner1);
        images.add( R.mipmap.banner2);
        images.add( R.mipmap.banner3);
        images.add( R.mipmap.banner2);

    }
    private void populateData() {
        List<SubListItems> services_category = new ArrayList<SubListItems>();
        List<SubListItems> popular_services = new ArrayList<SubListItems>();
        List<SubListItems> everyday_services = new ArrayList<SubListItems>();
        List<SubListItems> monthly_services = new ArrayList<SubListItems>();

        services_category.clear();
        SubListItems service_category = new SubListItems(R.mipmap.ser_1,"Electrician", null,null,"yes");
        services_category.add(service_category);
        service_category = new SubListItems(R.mipmap.ser_2,"Construction", null, null,"yes");
        services_category.add(service_category);
        service_category = new SubListItems(R.mipmap.ser_3,"Ac Repairing", null, null,"yes");
        services_category.add(service_category);
        service_category = new SubListItems(R.mipmap.ser_4,"Painter", null, null,"yes");
        services_category.add(service_category);
        service_category = new SubListItems(R.mipmap.ser_2,"Plumbling", null, null,"yes");
        services_category.add(service_category);

        popular_services.clear();
        SubListItems popular = new SubListItems(R.mipmap.services_1,"AC Services","AC services starting from 99 AED",null,null);
        popular_services.add(popular);
        popular = new SubListItems(R.mipmap.services_2,"Hanyman Services","Handyman services starting from 150 AED",null,null);
        popular_services.add(popular);
        popular = new SubListItems(R.mipmap.services_3,"Electrician Services","Electrician services starting from 150 AED",null,null);
        popular_services.add(popular);

        everyday_services.clear();
        SubListItems everyday = new SubListItems(R.mipmap.services_1,"Cleaning","Cleaning services starting from 99 AED",null,null);
        everyday_services.add(everyday);
        everyday = new SubListItems(R.mipmap.services_2,"Maid Services","Maid services starting from 150 AED",null,null);
        everyday_services.add(everyday);
        everyday = new SubListItems(R.mipmap.services_3,"Plumbing","Plumbing services starting from 150 AED",null,null);
        everyday_services.add(everyday);

        monthly_services.clear();
        SubListItems monthly = new SubListItems(R.mipmap.services_1,"Easy Pack","Easy Pack starting from 99 AED",null,null);
        monthly_services.add(monthly);
        monthly = new SubListItems(R.mipmap.services_1,"Eco Pack","Eco Pack starting from 99 AED",null,null);
        monthly_services.add(monthly);
        monthly = new SubListItems(R.mipmap.services_1,"Gold Pack","Gold Pack starting from 99 AED",null,null);
        monthly_services.add(monthly);

        newSublist.clear();
        Sublist sublist = new Sublist(true, "Services Categories", services_category);
        newSublist.add(sublist);
        sublist = new Sublist(false, "Popular Services", popular_services);
        newSublist.add(sublist);
        sublist = new Sublist(false, "Everyday Use", everyday_services);
        newSublist.add(sublist);
        sublist = new Sublist(false, "Monthly Bundles", monthly_services);
        newSublist.add(sublist);

    }

    @Override
    public void onPause() {
        super.onPause();
         swipeTimer.cancel();
    }
}
