package co.shapen.servasapp.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import co.shapen.servasapp.Adapter.RecycleViewAllGridItemsAdapter;
import co.shapen.servasapp.Adapter.RecycleViewCategoryGridItemsAdapter;
import co.shapen.servasapp.Models.SubListItems;
import co.shapen.servasapp.R;
import co.shapen.servasapp.interfaces.CustomClick;

/**
 * Created by Shapen on 7/25/2017.
 */

public class FragmentCategories extends Fragment {

    List<SubListItems> subListItems = new ArrayList<SubListItems>();
    RecyclerView recyclerView;
    CustomClick customClick;
    View view;
    RecycleViewCategoryGridItemsAdapter adapter;
    TextView tv;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_view_all, container, false);

        populateData();
        linkXml(view);
        return view;

    }

    private void linkXml(View view) {
        int numberOfColumns = 3;
        customClick = new CustomClick() {
            @Override
            public void setOnClickListner(View v, int position) {
//                getFragmentManager().beginTransaction()
//                        .replace(R.id.fragmentlayout, new FragmentProductDetails())
//                        .addToBackStack("")
//                        .commitAllowingStateLoss();
            }
        };
        recyclerView = (RecyclerView) view.findViewById(R.id.recycleview_grid);
        tv = (TextView) view.findViewById(R.id.product_title);

        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), numberOfColumns));
        adapter = new RecycleViewCategoryGridItemsAdapter(getActivity(), subListItems, customClick);
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListner(customClick);

        tv.setText("Categories");

    }

    private void populateData() {

        subListItems.clear();
        SubListItems category = new SubListItems(R.mipmap.cat1, "Fashion", null, null, null);
        subListItems.add(category);
        category = new SubListItems(R.mipmap.cat2, "Mobiles", null, null, null);
        subListItems.add(category);
        category = new SubListItems(R.mipmap.cat3, "Electronics", null, null, null);
        subListItems.add(category);
        category = new SubListItems(R.mipmap.cat4, "Gifts", null, null, null);
        subListItems.add(category);
        category = new SubListItems(R.mipmap.cat5, "Jewellery", null, null, null);
        subListItems.add(category);
        category = new SubListItems(R.mipmap.cat6, "Home Living", null, null, null);
        subListItems.add(category);
        category = new SubListItems(R.mipmap.cat7, "Beauty & \n Personal Care", null, null, null);
        subListItems.add(category);
        category = new SubListItems(R.mipmap.cat8, "Watches", null, null, null);
        subListItems.add(category);
        category = new SubListItems(R.mipmap.cat9, "Gaming", null, null, null);
        subListItems.add(category);
        category = new SubListItems(R.mipmap.cat4, "Gifts", null, null, null);
        subListItems.add(category);
        category = new SubListItems(R.mipmap.cat5, "Jewellery", null, null, null);
        subListItems.add(category);
        category = new SubListItems(R.mipmap.cat6, "Home Living", null, null, null);
        subListItems.add(category);
        category = new SubListItems(R.mipmap.cat7, "Beauty & \n Personal Care", null, null, null);
        subListItems.add(category);
        category = new SubListItems(R.mipmap.cat8, "Watches", null, null, null);
        subListItems.add(category);
        category = new SubListItems(R.mipmap.cat9, "Gaming", null, null, null);
        subListItems.add(category);

    }
}
