package co.shapen.servasapp.Fragments.MenuFragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import co.shapen.servasapp.Adapter.RecycleOrderAdapter;
import co.shapen.servasapp.Models.SubListItems;
import co.shapen.servasapp.R;
import co.shapen.servasapp.interfaces.CustomClick;

/**
 * Created by Shapen on 8/8/2017.
 */

public class FragmentOrderItems extends Fragment{
    RecyclerView recyclerView;
    View view;
    RecycleOrderAdapter adapter;
    TextView tv;
    CustomClick customClick = new CustomClick() {
        @Override
        public void setOnClickListner(View v, int position) {

        }
    };
    List<SubListItems> data = new ArrayList<SubListItems>();
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_order_details, container, false);
        linkXml(view);
        populateData();
        return view;
    }



    private void linkXml(View view) {

        recyclerView = (RecyclerView) view.findViewById(R.id.my_order_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        adapter = new RecycleOrderAdapter(getActivity(), data, customClick );
        adapter.setOnItemClickListner(customClick);
        recyclerView.setAdapter(adapter);
        //recyclerView.setAdapter(new RecycleOrderAdapter(getActivity(),data));


    }


    private void populateData() {
        data.clear();
        SubListItems dataitems = new SubListItems(R.mipmap.best_seller1,"Lorem ipsum dolor sit amet, consectetur adipiscing elit",
                "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour",
                "AED 250", "The standard chunk of Lorem Ipsum used since the 1500s");
        data.add(dataitems);
        dataitems = new SubListItems(R.mipmap.best_seller4,"Lorem ipsum dolor sit amet, consectetur adipiscing elit",
                "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour",
                "AED 250", "The standard chunk of Lorem Ipsum used since the 1500s");
        data.add(dataitems);
        dataitems = new SubListItems(R.mipmap.feature_3,"Lorem ipsum dolor sit amet, consectetur adipiscing elit",
                "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour",
                "AED 250", "The standard chunk of Lorem Ipsum used since the 1500s");
        data.add(dataitems);
        data.add(dataitems);
        dataitems = new SubListItems(R.mipmap.feature_2,"Lorem ipsum dolor sit amet, consectetur adipiscing elit",
                "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour",
                "AED 250", "The standard chunk of Lorem Ipsum used since the 1500s");
        data.add(dataitems);
        data.add(dataitems);
        dataitems = new SubListItems(R.mipmap.feature_4,"Lorem ipsum dolor sit amet, consectetur adipiscing elit",
                "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour",
                "AED 250", "The standard chunk of Lorem Ipsum used since the 1500s");
        data.add(dataitems);
        dataitems = new SubListItems(R.mipmap.pro_cat3,"Lorem ipsum dolor sit amet, consectetur adipiscing elit",
                "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour",
                "AED 250", "The standard chunk of Lorem Ipsum used since the 1500s");
        data.add(dataitems);

    }
}
