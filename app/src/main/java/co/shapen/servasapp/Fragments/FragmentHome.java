package co.shapen.servasapp.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.viewpagerindicator.CirclePageIndicator;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import co.shapen.servasapp.Activities.MenuActivity;
import co.shapen.servasapp.Adapter.RecycleHomeItemsAdapter;
import co.shapen.servasapp.Models.SubListItems;
import co.shapen.servasapp.Models.Sublist;
import co.shapen.servasapp.R;
import co.shapen.servasapp.Utils.SlidingImage_Adapter;
import co.shapen.servasapp.interfaces.CustomClick;
import co.shapen.servasapp.interfaces.CustomOnitemClick;

/***
 * Created by Shapen on 7/21/2017.
***/
public class FragmentHome extends Fragment {
    View view;
    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    CirclePageIndicator indicator;
    Bundle bundle;
    Timer swipeTimer;
    ArrayList images = new ArrayList<>();
    List<SubListItems> serviceList = new ArrayList<SubListItems>();
    // private static final Integer[] IMAGES= {R.mipmap.banner1,R.mipmap.banner2,R.mipmap.banner3,R.mipmap.banner2};
    private ArrayList<Integer> ImagesArray = new ArrayList<Integer>();
    List<Sublist> newSublist = new ArrayList<Sublist>();
    RecyclerView recyclerView;
    CustomOnitemClick customOnitemClick =new CustomOnitemClick() {
        @Override
        public void setOnItemClickListner(View v, int position) {
            FragmentManager fm = getFragmentManager();
            if (newSublist.get(position).getItemTitle().equals("Services")){
                fm.beginTransaction().replace(R.id.fragmentlayout, new FragmentServicesDetails()).addToBackStack("Fragment Services").commit();
            }
            else {
                getActivity().findViewById(R.id.tablayout).setVisibility(View.GONE);
                fm.beginTransaction().replace(R.id.fragmentlayout, new FragmentProductDetails()).addToBackStack("Fragment Services").commit();
            }


        }
    };
    CustomClick customClick = new CustomClick() {
        @Override
        public void setOnClickListner(View v, int position) {
            FragmentManager fm = getFragmentManager();
            if (newSublist.get(position).getItemTitle().equals("Feature Brands")){
                fm.beginTransaction().replace(R.id.fragmentlayout, new FragmentBrands()).addToBackStack("Fragment Brands").commit();
            }
            if (newSublist.get(position).getItemTitle().equals("Category")){
                fm.beginTransaction().replace(R.id.fragmentlayout, new FragmentCategories()).addToBackStack("Fragment Categories").commit();
            }
            if (newSublist.get(position).getItemTitle().equals("Services")){
                fm.beginTransaction().replace(R.id.fragmentlayout, new FragmentServices()).addToBackStack("Fragment Services").commit();
            }
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);

        populateData();
        linkXML(view);
        init();
        onClickListerns();
        return view;
    }

    private void linkXML(View view) {
        getActivity().findViewById(R.id.tablayout).setVisibility(View.VISIBLE);
        mPager = (ViewPager) view.findViewById(R.id.pager);
        indicator = (CirclePageIndicator) view.findViewById(R.id.indicator);
        recyclerView = (RecyclerView) view.findViewById(R.id.list_home_items);
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));

    }

    private void init() {
        populatedImages();
        currentPage = 0;
        for(int i=0;i<images.size();i++)
           // ImagesArray.add([i]);

        mPager.setAdapter(new SlidingImage_Adapter(getActivity(),images));
        indicator.setViewPager(mPager);
        //Set circle indicator radius
        final float density = getResources().getDisplayMetrics().density;
        indicator.setRadius(5 * density);
        images.size();
        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage ==  images.size()) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);
        System.out.println("purge value :"+swipeTimer.purge());

        RecycleHomeItemsAdapter adapter = new RecycleHomeItemsAdapter(getActivity(),newSublist, customClick,customOnitemClick);
        recyclerView.setAdapter(adapter);
      //  adapter.setOnItemClickListner(customOnitemClick);
    }

    private void onClickListerns() {
        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                if (currentPage==0) {
                    currentPage = position;
                }
            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });
    }


    public void populatedImages(){
        images.clear();

        images.add(R.mipmap.banner1);
        images.add( R.mipmap.banner2);
        images.add( R.mipmap.banner3);
        images.add( R.mipmap.banner2);

    }

    private void populateData() {
        List<SubListItems> featureList = new ArrayList<SubListItems>();
        List<SubListItems> categoryList = new ArrayList<SubListItems>();
        List<SubListItems> sellerList = new ArrayList<SubListItems>();
        List<SubListItems> brandList = new ArrayList<SubListItems>();

        featureList.clear();
        SubListItems features = new SubListItems(R.mipmap.feature_1,"Puma Elsu v2 Mid Cv IDP", "Up To 20% off", "AED 250","");
        featureList.add(features);
        features = new SubListItems(R.mipmap.feature_2,"Philips HDV4/01 Indus", "Up To 20% off", "AED 150","");
        featureList.add(features);
        features = new SubListItems(R.mipmap.feature_3,"Samsung Galaxy J5 - 6", "Up To 20% off", "AED 250","");
        featureList.add(features);
        features = new SubListItems(R.mipmap.feature_4,"Kotion Each GS410 Wired Head", "Up To 20% off", "AED 150","");
        featureList.add(features);

        categoryList.clear();
        SubListItems category = new SubListItems(R.mipmap.category_1,"Gadgets",null,null,null);
        categoryList.add(category);
        category = new SubListItems(R.mipmap.category_2,"Mobile",null,null,null);
        categoryList.add(category);
        category = new SubListItems(R.mipmap.category_3,"Fashion",null,null,null);
        categoryList.add(category);

        sellerList.clear();
        SubListItems sellers = new SubListItems(R.mipmap.best_seller1,"Puma Elsu v2 Mid Cv IDP", "Up To 20% off", "AED 250","");
        sellerList.add(sellers);
        sellers = new SubListItems(R.mipmap.best_seller2,"Philips HDV4/01 Indus", "Up To 20% off", "AED 150","");
        sellerList.add(sellers);
        sellers = new SubListItems(R.mipmap.best_seller3,"Samsung Galaxy J5 - 6", "Up To 20% off", "AED 250","");
        sellerList.add(sellers);
        sellers = new SubListItems(R.mipmap.best_seller4,"Kotion Each GS410 Wired Head", "Up To 20% off", "AED 150","");
        sellerList.add(sellers);

        serviceList.clear();
        SubListItems services = new SubListItems(R.mipmap.services_1,"Electrician",null,null,null);
        serviceList.add(services);
        services = new SubListItems(R.mipmap.services_2,"Plumbling",null,null,null);
        serviceList.add(services);
        services = new SubListItems(R.mipmap.services_3,"Mopping",null,null,null);
        serviceList.add(services);

        brandList.clear();
        SubListItems brands = new SubListItems(R.mipmap.feature_brand1,null,null,null,null);
        brandList.add(brands);
        brands = new SubListItems(R.mipmap.feature_brand2, null,null,null,null);
        brandList.add(brands);
        brands = new SubListItems(R.mipmap.feature_brand3,null,null,null,null);
        brandList.add(brands);

        newSublist.clear();
        Sublist sublist = new Sublist(false, "Feature Products", featureList);
        newSublist.add(sublist);
        sublist = new Sublist(true, "Category", categoryList);
        newSublist.add(sublist);
        sublist = new Sublist(true, "Services", serviceList);
        newSublist.add(sublist);
        sublist = new Sublist(false, "Best Sellers", sellerList);
        newSublist.add(sublist);
        sublist = new Sublist(true, "Feature Brands", brandList);
        newSublist.add(sublist);

    }

    @Override
    public void onPause() {
        super.onPause();
        swipeTimer.cancel();
    }
}
