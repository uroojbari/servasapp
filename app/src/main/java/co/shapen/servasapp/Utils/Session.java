package co.shapen.servasapp.Utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import java.util.HashMap;


/**
 * Created by urooj.khalid on 12/21/2016.
 */

public class Session {

    SharedPreferences pref;

    SharedPreferences.Editor editor;
    Context _context;

    int PRIVATE_MODE = 0;
    public static final String PREF_NAME = "PatientData";
    public static final String ID = "id";
    public static final String POSITION = "Position";
    public static final String ReviewID = "ReviewId";
    public static final String FirstNAME = "firstname";
    public static final String LastNAME = "lastname";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String AGE = "age";
    public static final String CONTACT = "contact";
    public static final String GENDER = "gender";
    public static final String COUNTRY = "country";
    public static final String IMAGE = "image";
    public static final String LONGITUDE = "longitude";
    public static final String LATITUDE = "latitude";
    public static final String DEVICE_TOKEN = "devicetoken";
    public static final String IS_LOGIN = "IsLoggedIn";


    // Constructor
    public Session(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }


    public void createSignUpSession(String firstname,String lastname, String email,
                                    String contact, String gender, String password,String age, String country,
                                    String image,String longitude,String latitude,String devicetoken,String user_id){

        editor.putBoolean(IS_LOGIN, true);
        editor.putString(ID,user_id);
        editor.putString(FirstNAME,firstname);
        editor.putString(LastNAME,lastname);
        editor.putString(EMAIL,email);
        editor.putString(PASSWORD,password);
        editor.putString(AGE,age);
        editor.putString(CONTACT,contact);
        editor.putString(GENDER,gender);
        editor.putString(COUNTRY,country);
        editor.putString(IMAGE,image);
        editor.putString(LONGITUDE,longitude);
        editor.putString(LATITUDE,latitude);
        editor.putString(DEVICE_TOKEN,devicetoken);

        editor.commit();

//        Intent i = new Intent(_context, PatientsActivity.class);
//        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        _context.startActivity(i);

    }


    public void createUserSession(String firstname,String lastname, String email,
                                  String contact, String gender, String password,String age, String country,
                                  String image,String longitude,String latitude,String devicetoken,String user_id){

        editor.putBoolean(IS_LOGIN, true);
        editor.putString(ID,user_id);
        editor.putString(FirstNAME,firstname);
        editor.putString(LastNAME,lastname);
        editor.putString(EMAIL,email);
        editor.putString(PASSWORD,password);
        editor.putString(AGE,age);
        editor.putString(CONTACT,contact);
        editor.putString(GENDER,gender);
        editor.putString(COUNTRY,country);
        editor.putString(IMAGE,image);
        editor.putString(LONGITUDE,longitude);
        editor.putString(LATITUDE,latitude);
        editor.putString(DEVICE_TOKEN,devicetoken);

        editor.commit();

//        Intent i = new Intent(_context, PatientsActivity.class);
//        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        _context.startActivity(i);
    }

    /**
     * Check login method wil check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     * */
    public boolean checkLogin(){
        // Check login status
        if(!this.isLoggedIn()){
            // user is not logged in redirect him to Login Activity

            // Fragment fm = new Fragment();
            //fm.startActivity(new Intent(_context, Login.class));
        /*    Intent i = new Intent(_context, BothLogin.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            _context.startActivity(i);*/
            return true;
        }

        return false;
    }



    /**
     * Get stored session data
     * */
    public HashMap<String, String> getPatientDetails(){
        HashMap<String, String> user = new HashMap<String, String>();
        // user name

        user.put(ID,pref.getString(ID,null));
        user.put(FirstNAME,pref.getString(FirstNAME,null));
        user.put(LastNAME,pref.getString(LastNAME,null));
        user.put(EMAIL, pref.getString(EMAIL, null));
        user.put(PASSWORD,pref.getString(PASSWORD,null));
        user.put(CONTACT,pref.getString(CONTACT,null));
        user.put(AGE, pref.getString(AGE,null));
        user.put(GENDER,pref.getString(GENDER,null));
        user.put(COUNTRY,pref.getString(COUNTRY,null));
        user.put(IMAGE,pref.getString(IMAGE,null));
        user.put(LONGITUDE,pref.getString(LONGITUDE,null));
        user.put(LATITUDE,pref.getString(LATITUDE,null));
        user.put(DEVICE_TOKEN,pref.getString(DEVICE_TOKEN,null));

        return user;
    }


    public HashMap<String, String> getPatientId(){
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(ID,pref.getString(ID,null));
        return user;
    }

    public void position(int position){

        editor.putInt(POSITION, position);
        editor.commit();


    }

    public HashMap<String, String> getReviewId(){
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(ID,pref.getString(ID,null));
        return user;
    }
    /**
     * Clear session details
     * */
    public void logoutUser(){
        editor.clear();
        editor.commit();

//        Intent i =new Intent(_context, BothLogin.class);
//        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        _context.startActivity(i);
    }

    public boolean isLoggedIn(){
        return pref.getBoolean(IS_LOGIN, false);
    }



}
