package co.shapen.servasapp.Models;

/**
 * Created by Shapen on 7/24/2017.
 */

public class DrawerItems {

    Integer images;
    String title;

    public DrawerItems (String title, Integer images){

        this.images = images;
        this.title = title;

    }

    public Integer getImages() {
        return images;
    }

    public void setImages(Integer images) {
        this.images = images;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


}
