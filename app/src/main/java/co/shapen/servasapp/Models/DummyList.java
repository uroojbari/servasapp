package co.shapen.servasapp.Models;

import java.util.List;

/**
 * Created by Shapen on 7/25/2017.
 */

public class DummyList {

    List<Sublist> sublists;

    public DummyList(List<Sublist> list){

        this.sublists = list;

    }

    public List<Sublist> getSublists() {
        return sublists;
    }

    public void setSublists(List<Sublist> sublists) {
        this.sublists = sublists;
    }




}
