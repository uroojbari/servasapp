package co.shapen.servasapp.Models;

/**
 * Created by Shapen on 7/25/2017.
 */

public class SubListItems {

    Integer image;
    String text;
    String price;
    String description;
    String title;

    public SubListItems(Integer image, String title, String description, String price, String text){

        this.image = image;
        this.text = text;
        this.price = price;
        this.description = description;
        this.title = title;

    }
    public SubListItems(Integer image, String text){
        this.image = image;
        this.text = text;
    }

    public SubListItems(String title,String text){
        this.text = text;
        this.title = title;
    }

    public SubListItems(String title,String text, String price){
        this.text = text;
        this.title = title;
        this.price = price;
    }
    public Integer getImage() {
        return image;
    }

    public void setImage(Integer image) {
        this.image = image;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
