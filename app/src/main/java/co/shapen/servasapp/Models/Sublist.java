package co.shapen.servasapp.Models;

import java.util.List;

/**
 * Created by Shapen on 7/25/2017.
 */

public class Sublist {
    boolean views;
    String itemtitle;

    public Sublist(boolean views, String itemtitle, List<SubListItems> subListItems){

        this.views = views;
        this.itemtitle = itemtitle;
        this.subListItems = subListItems;

    }

    List<SubListItems> subListItems;
    public boolean isViews() {
        return views;
    }

    public void setViews(boolean views) {
        this.views = views;
    }

    public String getItemTitle() {
        return itemtitle;
    }

    public void setItemTitle(String itemtitle) {
        this.itemtitle = itemtitle;
    }

    public List<SubListItems> getSubListItems() {
        return subListItems;
    }

    public void setSubListItems(List<SubListItems> subListItems) {
        this.subListItems = subListItems;
    }


}
