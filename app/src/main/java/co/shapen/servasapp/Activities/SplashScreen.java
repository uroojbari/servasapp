package co.shapen.servasapp.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

//import com.gospelware.liquidbutton.LiquidButton;

import java.util.Timer;
import java.util.TimerTask;

import co.shapen.servasapp.R;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

//        LiquidButton liquidButton = (LiquidButton) findViewById(R.id.button);
//        liquidButton.startPour();
//        liquidButton.setEnabled(true);
//        liquidButton.setFillAfter(true);
//        liquidButton.setAutoPlay(true);
//        liquidButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                LiquidButton btn = (LiquidButton) v;
////                btn.startPour();
//            }
//        });

       // liquidButton.changeProgress(this);

//        liquidButton.setPourFinishListener(new LiquidButton.PourFinishListener() {
//            @Override
//            public void onPourFinish() {
//                startActivity(new Intent(SplashScreen.this, LoginActivity.class));
//                finish();
//               // Toast.makeText(MainActivity.this, "Finish", Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onProgressUpdate(float progress) {
//               /// textView.setText(String.format("%.2f", progress * 100) + "%");
//            }
//        });
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {

                startActivity(new Intent(SplashScreen.this, LoginActivity.class));
                finish();
            }
        },3000);

    }

}
