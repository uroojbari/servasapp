package co.shapen.servasapp.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.view.Gravity;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import co.shapen.servasapp.Adapter.DrawerMainMenuAdapter;
import co.shapen.servasapp.Fragments.FragmentHome;
import co.shapen.servasapp.Models.DrawerItems;
import co.shapen.servasapp.R;

public class MasterActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener, AdapterView.OnItemClickListener {

    List<DrawerItems> drawerItems = new ArrayList<DrawerItems>();
    ListView navigationlist;
    TabLayout tabLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_master);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        populateList();
        linkXML();
        onClickListren();

    }

    private void onClickListren() {
        findViewById(R.id.ic_drawer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.openDrawer(Gravity.START);

            }
        });
    }

    private void linkXML() {
        FragmentManager fm = getSupportFragmentManager();
        tabLayout = (TabLayout) findViewById(R.id.tablayout);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationlist = (ListView) navigationView.findViewById(R.id.navigationlist);
        navigationlist.setAdapter(new DrawerMainMenuAdapter(MasterActivity.this, drawerItems));
        navigationlist.setOnItemClickListener(this);
        userprofile(navigationView);


        tabLayout.addTab(tabLayout.newTab().setIcon(R.mipmap.ic_home).setText("Home"));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.mipmap.ic_deals).setText("Deals"));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.mipmap.ic_fav).setText("Services"));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.mipmap.ic_account).setText("History"));
        tabLayout.getTabAt(0);
        fm.beginTransaction().add(R.id.fragmentlayout,new FragmentHome()).commit();
        tabLayout.setOnTabSelectedListener(this);

    }

    private void userprofile(NavigationView navigationView) {

        ImageView userPic = (ImageView) navigationView.findViewById(R.id.user_pic);
        TextView userName = (TextView) navigationView.findViewById(R.id.user_name);
        TextView userTitle = (TextView) navigationView.findViewById(R.id.user_title);

        userName.setText("WELCOME RIYAZ");
        userTitle.setText("Login | Signup");
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        FragmentManager fm = getSupportFragmentManager();
        switch (tab.getPosition()){
            case 0:
                fm.beginTransaction().add(R.id.fragmentlayout,new FragmentHome()).commit();
              //  Toast.makeText(this, "Tab 1", Toast.LENGTH_SHORT).show();
                break;

            case 1:
//                fm.beginTransaction().add(R.id.fragmentlayout,new FragmentHome()).commit();
                //Toast.makeText(this, "Tab 2", Toast.LENGTH_SHORT).show();
                break;

            case 2:
//                fm.beginTransaction().add(R.id.fragmentlayout,new FragmentHome()).commit();
               // Toast.makeText(this, "Tab 3", Toast.LENGTH_SHORT).show();
                break;

            case 3:
//                fm.beginTransaction().add(R.id.fragmentlayout,new FragmentHome()).commit();
               // Toast.makeText(this, "Tab 4", Toast.LENGTH_SHORT).show();
                break;


        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
        FragmentManager fm = getSupportFragmentManager();
        switch (tab.getPosition()){
            case 0:
                fm.beginTransaction().add(R.id.fragmentlayout,new FragmentHome()).commit();
                Toast.makeText(this, "Tab 1", Toast.LENGTH_SHORT).show();
                break;

            case 1:
//                fm.beginTransaction().add(R.id.fragmentlayout,new FragmentHome()).commit();
                Toast.makeText(this, "Tab 2", Toast.LENGTH_SHORT).show();
                break;

            case 2:
//                fm.beginTransaction().add(R.id.fragmentlayout,new FragmentHome()).commit();
                Toast.makeText(this, "Tab 3", Toast.LENGTH_SHORT).show();
                break;

            case 3:
//                fm.beginTransaction().add(R.id.fragmentlayout,new FragmentHome()).commit();
                Toast.makeText(this, "Tab 4", Toast.LENGTH_SHORT).show();
                break;


        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent;
        Bundle bundle = new Bundle();

        switch (position){
            case 0:
                tabLayout.getTabAt(0);
                getSupportFragmentManager().beginTransaction().add(R.id.fragmentlayout,new FragmentHome()).commit();
//                intent = new Intent(MasterActivity.this, MenuActivity.class);
//                bundle.putString("Fragment","Profile Setting");
//                intent.putExtras(bundle);
//                startActivity(intent);
                break;


            case 1:
                intent = new Intent(MasterActivity.this, MenuActivity.class);
                bundle.putString("Fragment","Profile Settings");
                intent.putExtras(bundle);
                startActivity(intent);
                break;

            case 2:
                intent = new Intent(MasterActivity.this, MenuActivity.class);
                bundle.putString("Fragment","My Orders");
                intent.putExtras(bundle);
                startActivity(intent);
                break;

            case 3:
                intent = new Intent(MasterActivity.this, MenuActivity.class);
                bundle.putString("Fragment","My Bookings");
                intent.putExtras(bundle);
                startActivity(intent);
                break;

            case 4:
                intent = new Intent(MasterActivity.this, MenuActivity.class);
                bundle.putString("Fragment","Promo");
                intent.putExtras(bundle);
                startActivity(intent);
                break;

            case 5:
                intent = new Intent(MasterActivity.this, MenuActivity.class);
                bundle.putString("Fragment","Contact us");
                intent.putExtras(bundle);
                startActivity(intent);
                break;

            case 6:
                intent = new Intent(MasterActivity.this, MenuActivity.class);
                bundle.putString("Fragment","Help");
                intent.putExtras(bundle);
                startActivity(intent);
                break;

            case 7:
                intent = new Intent(MasterActivity.this, MenuActivity.class);
                bundle.putString("Fragment","Languages");
                intent.putExtras(bundle);
                startActivity(intent);
                break;

            case 8:
                intent = new Intent(MasterActivity.this, MenuActivity.class);
                bundle.putString("Fragment","Feedbacks");
                intent.putExtras(bundle);
                startActivity(intent);
                break;


            case 9:
                startActivity(new Intent(MasterActivity.this, LoginActivity.class));
                finish();
//                intent = new Intent(MasterActivity.this, MenuActivity.class);
//                bundle.putString("Fragment","Feedaback");
//                intent.putExtras(bundle);
//                startActivity(intent);
                break;


        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }


    private void populateList() {
        drawerItems.clear();

        DrawerItems item = new DrawerItems("Home", R.mipmap.menu_ic_home);
        drawerItems.add(item);

        item = new DrawerItems("Profile Setting", R.mipmap.menu_ic_profile);
        drawerItems.add(item);

        item = new DrawerItems("My Orders", R.mipmap.menu_ic_my_order);
        drawerItems.add(item);

        item = new DrawerItems("My Bookings", R.mipmap.menu_ic_booking);
        drawerItems.add(item);

        item = new DrawerItems("Promo", R.mipmap.menu_ic_promo);
        drawerItems.add(item);

        item = new DrawerItems("Contact us", R.mipmap.menu_ic_contact);
        drawerItems.add(item);

        item = new DrawerItems("Help", R.mipmap.menu_ic_help);
        drawerItems.add(item);

        item = new DrawerItems("Languages", R.mipmap.menu_ic_language);
        drawerItems.add(item);

        item = new DrawerItems("Feedback", R.mipmap.menu_ic_feedback);
        drawerItems.add(item);

        item = new DrawerItems("Logout", R.mipmap.menu_ic_logout);
        drawerItems.add(item);

    }

}