package co.shapen.servasapp.Activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.Arrays;
import co.shapen.servasapp.R;


public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {
    private static final int RC_SIGN_IN = 9001;
    Button login;
    GoogleApiClient mGoogleApiClient;
    CallbackManager callbackManager;
    LoginButton facebookSignin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        googleSignIn();
        onClickListren();
        FacebookSdk.sdkInitialize(getApplicationContext());
        facebookSignin = (LoginButton) findViewById(R.id.login_button);
    }

    private void onClickListren() {
        findViewById(R.id.btn_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, MasterActivity.class));
            }
        });

        findViewById(R.id.click_become_a_member).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
            }
        });

        findViewById(R.id.click_forgot_password).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class));
            }
        });
        findViewById(R.id.sign_in_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        });
        findViewById(R.id.login_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                facebookSignupMethod();

            }
        });

    }

    private void googleSignIn() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this , this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
        else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("TAG", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            Toast.makeText(this, acct.getDisplayName()+" "+ acct.getEmail(), Toast.LENGTH_SHORT).show();
          //  mStatusTextView.setText(getString(R.string.signed_in_fmt, acct.getDisplayName()));
           // updateUI(true);
        } else {
            Toast.makeText(this, result.getStatus().getStatusMessage()+"", Toast.LENGTH_SHORT).show();

        }
    }


    private void facebookSignupMethod() {
        facebookSignin.setReadPermissions(Arrays.asList("public_profile", "email",
                "user_birthday", "user_friends", "user_photos", "user_location"));
        callbackManager = CallbackManager.Factory.create();
        facebookSignin.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                AccessToken.getCurrentAccessToken().getToken();

                loginResult.getAccessToken().getUserId();
                loginResult.getAccessToken().getToken();

                GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        if (object != null) {
                            Toast.makeText(LoginActivity.this, "not Null", Toast.LENGTH_SHORT).show();
                            try {
                                String email = object.getString("email");
                                String locale = object.getString("locale");
                                String name = object.getString("name");
                                String fname = object.getString("first_name");
                                String lname = object.getString("last_name");
                                String gender = object.getString("gender");
                                String id = object.getString("id");
                                String age_range = object.getString("age_range");
                                String picture = object.getString("picture");
                                JSONObject object1 = new JSONObject(picture);
                                object1.getString("data");
                                String data = object1.getString("data");
                                JSONObject object2 = new JSONObject(data);
                                object2.getString("url");
                               // imageURL = object2.getString("url");
                                Toast.makeText(LoginActivity.this, email+" "+name+" ", Toast.LENGTH_SHORT).show();

                                //  ImageUri = Profile.getCurrentProfile().getProfilePictureUri(400,400);

//                                Validations.getBitmapFromURL(imageURL);
                                //   bitmap = Validations.getBitmapFromURL(imageURL);
                                //        File filem = new File(bitmap.toString());

                                //  file = new TypedFile("multipart/form-data",filem.getAbsoluteFile());
                              //  DialogPatientsDetailsFb(email, fname, lname, name, imageURL);

                                //    loginSession.createLoginSession(name,email,imageURL,gender,id);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
//2jmj7l5rSw0yVb/vlWAYkK/YBwk=
                        } else {
                            Toast.makeText(LoginActivity.this, "Null", Toast.LENGTH_SHORT).show();
                        }

                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "picture,id,age_range,name,locale,link,email,gender,birthday,location,first_name,last_name");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Toast.makeText(LoginActivity.this, "Request cancelled by the user", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(LoginActivity.this,error.toString()+"", Toast.LENGTH_SHORT).show();
                //  SnackBarUtils.showSnackBar(findViewById(android.R.id.content), error.toString(), Snackbar.LENGTH_LONG);
                Log.d("onError: ", error.toString());

            }
        });

    }
}
