package co.shapen.servasapp.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import co.shapen.servasapp.Fragments.FragmentProductDetails;
import co.shapen.servasapp.Fragments.MenuFragments.FragmentContactUs;
import co.shapen.servasapp.Fragments.MenuFragments.FragmentFeedback;
import co.shapen.servasapp.Fragments.MenuFragments.FragmentHelp;
import co.shapen.servasapp.Fragments.MenuFragments.FragmentLanguage;
import co.shapen.servasapp.Fragments.MenuFragments.FragmentMyBookings;
import co.shapen.servasapp.Fragments.MenuFragments.FragmentMyOrders;
import co.shapen.servasapp.Fragments.MenuFragments.FragmentProfileSetting;
import co.shapen.servasapp.Fragments.MenuFragments.FragmentPromo;
import co.shapen.servasapp.R;

public class MenuActivity extends AppCompatActivity {

    Bundle extras;
    String fragments;
    TextView header_title;
    RelativeLayout ic_back;
    RelativeLayout layout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        linkXML();
        extras = getIntent().getExtras();
        if (extras.getString("Fragment")!= null) {
            fragments =  extras.getString("Fragment");
            switching(fragments);
        }
    }

    private void linkXML() {
        header_title = (TextView) findViewById(R.id.header_title);
        ic_back = (RelativeLayout) findViewById(R.id.ic_back);
        layout = (RelativeLayout) findViewById(R.id.layout);
        ic_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    private void switching(String fragments) {

        if (fragments.equals("Profile Settings")) {

            header_title.setText("Profile Settings");
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.menu_fragment, new FragmentProfileSetting())
                    .commitAllowingStateLoss();
        }
        else if (fragments.equals("My Orders")) {
            header_title.setText("My Orders");
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.menu_fragment, new FragmentMyOrders())
                    .commitAllowingStateLoss();
        }
        else if (fragments.equals( "My Bookings")) {
            header_title.setText("My Bookings");
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.menu_fragment, new FragmentMyBookings())
                    .commitAllowingStateLoss();
        }
        else if (fragments.equals( "Promo")) {
            header_title.setText("Promo");
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.menu_fragment, new FragmentPromo())
                    .commitAllowingStateLoss();
        }
        else if (fragments.equals( "Contact us")) {
            header_title.setText("Contact us");
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.menu_fragment, new FragmentContactUs())
                    .commitAllowingStateLoss();
        }
        else if (fragments.equals( "Help")) {
            header_title.setText("Help");
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.menu_fragment, new FragmentHelp())
                    .commitAllowingStateLoss();
        }
        else if (fragments.equals( "Languages")) {
            header_title.setText("Languages");
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.menu_fragment, new FragmentLanguage())
                    .commitAllowingStateLoss();
        }
        else if (fragments.equals( "Feedbacks")) {
            header_title.setText("Feedbacks");
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.menu_fragment, new FragmentFeedback())
                    .commitAllowingStateLoss();
        }
        else if (fragments.equals( "Add to Cart")) {
            header_title.setText("");
            layout.setVisibility(View.GONE);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.menu_fragment, new FragmentProductDetails())
                    .commitAllowingStateLoss();
        }
        }



}
