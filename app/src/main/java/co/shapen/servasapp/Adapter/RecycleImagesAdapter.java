 package co.shapen.servasapp.Adapter;

 import android.app.Activity;
 import android.support.v7.widget.RecyclerView;
 import android.view.LayoutInflater;
 import android.view.View;
 import android.view.ViewGroup;
 import android.widget.ImageView;
 import android.widget.TextView;

 import java.util.List;

 import co.shapen.servasapp.Models.SubListItems;
 import co.shapen.servasapp.R;
 import co.shapen.servasapp.interfaces.CustomOnitemClick;

 /**
  * Created by urooj.khalid on ic_email/22/2017.
  */

 public class RecycleImagesAdapter extends RecyclerView.Adapter<RecycleImagesAdapter.ViewHolder> {
     private View view;
     ViewHolder viewHolder;
     Activity activity;
     public static CustomOnitemClick customOnitemClick;
     private List<SubListItems> subListItems;

     public RecycleImagesAdapter(Activity activity, List<SubListItems> subListItems, CustomOnitemClick customOnitemClick) {
         this.subListItems = subListItems;
         this.activity = activity;
         this.customOnitemClick = customOnitemClick;
     }

     public void setOnItemClickListern( CustomOnitemClick customOnitemClick){

         this.customOnitemClick = customOnitemClick;
     }

     @Override
     public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
         view = LayoutInflater.from(parent.getContext()).inflate(R.layout.showcaseimages_layout, parent, false);
         viewHolder = new ViewHolder(view);
         return viewHolder;
     }

     @Override
     public void onBindViewHolder(ViewHolder holder, final int position) {
         SubListItems sublist1 = subListItems.get(position);
         holder.showcase_image.setImageResource(sublist1.getImage());
     }

     @Override
     public int getItemCount() {
         return subListItems.size();
     }

     public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

         TextView tv, tv2;
         ImageView showcase_image;
         public ViewHolder(View itemView) {
             super(itemView);
             itemView.setOnClickListener(this);
             showcase_image = (ImageView) itemView.findViewById(R.id.showcase_image);
         }

         @Override
         public void onClick(View v) {
             if (customOnitemClick!=null){
                 customOnitemClick.setOnItemClickListner(v, getPosition());

             }
         }
     }
 }
