package co.shapen.servasapp.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import co.shapen.servasapp.Models.SubListItems;
import co.shapen.servasapp.R;


/**
 * Created by urooj.khalid on ic_user/9/2017.
 */

public class GridItemsAdapter extends BaseAdapter {
    Activity activity;
    List<SubListItems> subListItemses;
    String [] items = {""};
    View row;

    public GridItemsAdapter(Activity activity, List<SubListItems> subListItems) {
        this.activity = activity ;
        this.subListItemses = subListItems;
    }

    @Override
    public int getCount() {
        return subListItemses.size();
    }

    @Override
    public Object getItem(int position) {
        return subListItemses.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null){
            LayoutInflater inflater= (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.custom_griditems,parent,false);
        }

        else {
            row=convertView;
        }

        ImageView image = (ImageView) row.findViewById(R.id.grid_images);
        TextView title = (TextView) row.findViewById(R.id.grid_titles);

        image.setImageResource(subListItemses.get(position).getImage());

        if (subListItemses.get(position).getTitle()!=null){
            title.setText(subListItemses.get(position).getTitle());
        }
        else {

            title.setVisibility(View.GONE);
        }

        return row;
    }
}
