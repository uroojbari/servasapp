 package co.shapen.servasapp.Adapter;

 import android.app.Activity;
 import android.support.v7.widget.RecyclerView;
 import android.view.LayoutInflater;
 import android.view.View;
 import android.view.ViewGroup;
 import android.widget.TextView;

 import java.util.List;

 import co.shapen.servasapp.Models.SubListItems;
 import co.shapen.servasapp.R;

 /**
  * Created by urooj.khalid on ic_email/22/2017.
  */

 public class RecycleBookingAdapter extends RecyclerView.Adapter<RecycleBookingAdapter.ViewHolder> {
     private View view;
     ViewHolder viewHolder;
     Activity activity;
     private List<SubListItems> subListItems;

     public RecycleBookingAdapter(Activity activity, List<SubListItems> subListItems) {
         this.subListItems = subListItems;
         this.activity = activity;
     }

     @Override
     public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
         view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_booking_layout, parent, false);
         viewHolder = new ViewHolder(view);
         return viewHolder;
     }

     @Override
     public void onBindViewHolder(ViewHolder holder, final int position) {
         SubListItems sublist1 = subListItems.get(position);
         holder.tv.setText(sublist1.getTitle());
         holder.tv2.setText(sublist1.getText());
     }

     @Override
     public int getItemCount() {
         return subListItems.size();
     }

     public static class ViewHolder extends RecyclerView.ViewHolder {

         TextView tv, tv2;
         public ViewHolder(View itemView) {
             super(itemView);
             tv = (TextView) itemView.findViewById(R.id.booking_main_loc);
             tv2 = (TextView) itemView.findViewById(R.id.booking_user_loc);
         }
     }
 }
