package co.shapen.servasapp.Adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import co.shapen.servasapp.Models.SubListItems;
import co.shapen.servasapp.Models.Sublist;
import co.shapen.servasapp.R;
import co.shapen.servasapp.interfaces.CustomClick;

/**
 * Created by urooj.khalidon ic_email/22/2017.
 */

public class RecycleViewCategoryGridItemsAdapter extends RecyclerView.Adapter<RecycleViewCategoryGridItemsAdapter.ViewHolder> {
    private View view;
    ViewHolder viewHolder;
    Activity activity;

    private List<SubListItems> sublist;
    List<Sublist> data;
    public static CustomClick customClick;

    public RecycleViewCategoryGridItemsAdapter(Activity activity, List<SubListItems> sublist, CustomClick customClick) {
        this.sublist = sublist;
        this.activity = activity;
        this.customClick = customClick;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_griditems, parent, false);
        viewHolder = new ViewHolder(view);
        return viewHolder;
    }
    public void setOnItemClickListner (CustomClick onItemClickListner){

        this.customClick = onItemClickListner;
    }
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        SubListItems sublist1 = sublist.get(position);
        holder.tvName.setText(sublist1.getTitle());
        if (sublist1.getText()!= null) {
            holder.imageView.setImageResource(sublist1.getImage());
        //    holder.imageView.setBackgroundResource(R.drawable.shape_circle);
          //  holder.imageView.setPadding(25,25,25,25);

        }
        else {
            holder.imageView.setImageResource(sublist1.getImage());
          //  holder.imageView.setPadding(25,25,25,25);
        }
      //  holder.imageView.setImageResource(sublist1.getImage());
    }


    @Override
    public int getItemCount() {
        return sublist.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvName, tvDetail, tvPrice;
        ImageView imageView;



        public ViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.grid_titles);
            imageView = (ImageView) itemView.findViewById(R.id.grid_images);
            itemView.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {
            if (customClick!=null){

                customClick.setOnClickListner(v,getPosition());

            }
        }
    }
}
