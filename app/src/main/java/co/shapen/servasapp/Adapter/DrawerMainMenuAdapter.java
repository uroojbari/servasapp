package co.shapen.servasapp.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import co.shapen.servasapp.Models.DrawerItems;
import co.shapen.servasapp.R;


/**
 * Created by urooj.khalid on ic_user/9/2017.
 */

public class DrawerMainMenuAdapter extends BaseAdapter {
    Activity activity;
    List<DrawerItems> drawerItems = new ArrayList<DrawerItems>();

    public DrawerMainMenuAdapter(Activity activity, List<DrawerItems> items){
        this.activity = activity ;
        this.drawerItems = items ;
    }

    @Override
    public int getCount() {
        return drawerItems.size();
    }

    @Override
    public Object getItem(int position) {
        return drawerItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row= null;
        if (convertView == null){

            LayoutInflater inflater= (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row=inflater.inflate(R.layout.custom_drawer_items,parent,false);
        }

        else {
            row=convertView;
        }

        TextView textView= (TextView) row.findViewById(R.id.drawer_titles);
        ImageView image = (ImageView) row.findViewById(R.id.drawer_icons);
        textView.setText(drawerItems.get(position).getTitle());
        image.setImageResource(drawerItems.get(position).getImages());

        return row;
    }
}
