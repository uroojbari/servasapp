package co.shapen.servasapp.Adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import co.shapen.servasapp.Models.Sublist;
import co.shapen.servasapp.R;
import co.shapen.servasapp.interfaces.CustomClick;
import co.shapen.servasapp.interfaces.CustomOnitemClick;

/**
 * Created by urooj.khalid on ic_email/22/2017.
 */

public class RecycleHomeItemsAdapter extends RecyclerView.Adapter<RecycleHomeItemsAdapter.ViewHolder> {
    private View view;
    ViewHolder viewHolder;
    Activity activity;
    private List<Sublist> sublist;
    public static CustomOnitemClick customOnitemClick;
    public static CustomClick customClick;
    public RecycleHomeItemsAdapter(Activity activity, List<Sublist> sublist, CustomClick customClick, CustomOnitemClick customOnitemClick) {
        this.sublist = sublist;
        this.activity = activity;
        this.customClick = customClick;
        this.customOnitemClick = customOnitemClick;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_listtiem, parent, false);
        viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Sublist sublist1 = sublist.get(position);
        holder.tvName.setText(sublist1.getItemTitle());
        if(sublist1.isViews()== true){
            holder.tvDetail.setText("View All");
            holder.tvDetail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    customClick.setOnClickListner(v,position);
                }
            });
        }
        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        float density = dm.density;
        int size=sublist1.getSubListItems().size();
        // Calculated single Item Layout Width for each grid element ....
        int width = 150;
        int totalWidth = (int) (width * size * density);
        int singleItemWidth = (int) (width * density);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                totalWidth, LinearLayout.LayoutParams.WRAP_CONTENT);
        holder.gridView.setAdapter(new GridHomeItemsAdapter(activity,sublist1.getSubListItems()));
        holder.gridView.setLayoutParams(params);
        holder.gridView.setColumnWidth(singleItemWidth);
//        holder.gridView.setStretchMode(GridView.STRETCH_SPACING);
        holder.gridView.setNumColumns(sublist1.getSubListItems().size());

        holder.gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int p, long id) {
                if (customOnitemClick!=null){

                    customOnitemClick.setOnItemClickListner(view,position);

                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return sublist.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvName, tvDetail;
        GridView gridView;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tv_title);
            tvDetail = (TextView) itemView.findViewById(R.id.tv_view_all);
            gridView = (GridView) itemView.findViewById(R.id.gridview);
        }


    }
}
