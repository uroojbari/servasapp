package co.shapen.servasapp.Adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import co.shapen.servasapp.Models.SubListItems;
import co.shapen.servasapp.Models.Sublist;
import co.shapen.servasapp.R;
import co.shapen.servasapp.interfaces.CustomClick;

/**
 * Created by urooj.khalid on ic_email/22/2017.
 */

public class RecycleViewAllGridItemsAdapter extends RecyclerView.Adapter<RecycleViewAllGridItemsAdapter.ViewHolder> {
    private View view;
    ViewHolder viewHolder;
    Activity activity;

    private List<SubListItems> sublist;
    List<Sublist> data;
    CustomClick customClick;

    public RecycleViewAllGridItemsAdapter(Activity activity, List<SubListItems> sublist, CustomClick customClick) {
        this.sublist = sublist;
        this.activity = activity;
        this.customClick = customClick;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_griditem_viewall, parent, false);
        viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        SubListItems sublist1 = sublist.get(position);
        holder.tvName.setText(sublist1.getTitle());
        holder.imageView.setImageResource(sublist1.getImage());
    }


    @Override
    public int getItemCount() {
        return sublist.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvName, tvDetail, tvPrice;
        ImageView imageView;


        public ViewHolder(View itemView) {
            super(itemView);

            tvName = (TextView) itemView.findViewById(R.id.grid_title);
            tvDetail = (TextView) itemView.findViewById(R.id.grid_description);
            tvPrice = (TextView) itemView.findViewById(R.id.grid_price);
            imageView = (ImageView) itemView.findViewById(R.id.grid_image);


        }
    }
}
