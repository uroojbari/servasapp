package co.shapen.servasapp.Adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import co.shapen.servasapp.Models.SubListItems;
import co.shapen.servasapp.Models.Sublist;
import co.shapen.servasapp.R;
import co.shapen.servasapp.interfaces.CustomClick;

/**
 * Created by urooj.khalid on ic_email/22/2017.
 */

public class RecycleViewGridInnerCats extends RecyclerView.Adapter<RecycleViewGridInnerCats.ViewHolder> {
    private View view;
    ViewHolder viewHolder;
    Activity activity;

    private List<SubListItems> sublist;
    List<Sublist> data;
    public static CustomClick customClick;

    public RecycleViewGridInnerCats(Activity activity, List<SubListItems> sublist, CustomClick customClick) {
        this.sublist = sublist;
        this.activity = activity;
        this.customClick = customClick;
    }
    public void setOnitemClickLIstner(CustomClick customClick){
        this.customClick = customClick;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_inner_cats, parent, false);
        viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        SubListItems sublist1 = sublist.get(position);
        holder.tvDetail.setText(sublist1.getTitle());
        holder.imageView.setImageResource(sublist1.getImage());
    }


    @Override
    public int getItemCount() {
        return sublist.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvName, tvDetail;
        ImageView imageView;


        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            tvDetail = (TextView) itemView.findViewById(R.id.inner_cat_text);
            imageView = (ImageView) itemView.findViewById(R.id.inner_cat_image);


        }

        @Override
        public void onClick(View v) {
            if (customClick!=null){

                customClick.setOnClickListner(v,getPosition());

            }
        }
    }
}
