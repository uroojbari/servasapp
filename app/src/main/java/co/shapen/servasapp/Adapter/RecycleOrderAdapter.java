 package co.shapen.servasapp.Adapter;

 import android.app.Activity;
 import android.support.v7.widget.RecyclerView;
 import android.view.LayoutInflater;
 import android.view.View;
 import android.view.ViewGroup;
 import android.widget.ImageView;
 import android.widget.TextView;

 import java.util.List;

 import co.shapen.servasapp.Models.SubListItems;
 import co.shapen.servasapp.R;
 import co.shapen.servasapp.interfaces.CustomClick;

 /**
  * Created by urooj.khalid on ic_email/22/2017.
  */

 public class RecycleOrderAdapter extends RecyclerView.Adapter<RecycleOrderAdapter.ViewHolder> {
     private View view;
     ViewHolder viewHolder;
     private static CustomClick customClick;
     Activity activity;
     private List<SubListItems> subListItems;

     public RecycleOrderAdapter(Activity activity, List<SubListItems> subListItems, CustomClick customClick) {
         this.subListItems = subListItems;
         this.activity = activity;
         this.customClick = customClick;
     }

     @Override
     public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
         view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_orders_layout, parent, false);
         viewHolder = new ViewHolder(view);
         return viewHolder;
     }

     public void setOnItemClickListner (CustomClick onItemClickListner){

         this.customClick = onItemClickListner;
     }

     @Override
     public void onBindViewHolder(ViewHolder holder, final int position) {
         SubListItems sublist1 = subListItems.get(position);

         holder.title.setText(sublist1.getTitle());
         holder.description.setText(sublist1.getDescription());
         holder.text.setText(sublist1.getTitle());
         holder.price.setText(sublist1.getPrice());
         holder.image.setImageResource(sublist1.getImage());
     }

     @Override
     public int getItemCount() {
         return subListItems.size();
     }

     public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

         TextView title, text, description, price;
         ImageView image;
         public ViewHolder(View itemView) {
             super(itemView);
             itemView.setOnClickListener(this);
             title = (TextView) itemView.findViewById(R.id.prod_title);
             text = (TextView) itemView.findViewById(R.id.prod_text);
             description = (TextView) itemView.findViewById(R.id.prod_description);
             price = (TextView) itemView.findViewById(R.id.prod_price);
             image = (ImageView) itemView.findViewById(R.id.prod_image);
         }

         @Override
         public void onClick(View v) {
             if (customClick!=null){

                 customClick.setOnClickListner(v,getPosition());

             }
         }
     }
 }
